curl 'https://pierpestregister.mpi.govt.nz/PestsRegister/ImportCommodity/GetTableDataAjax' \
-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0' \
-H 'Accept: application/json, text/javascript, */*; q=0.01' \
-H 'Accept-Language: en-US,en;q=0.5' --compressed \
-H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' \
-H 'RequestVerificationToken: CfDJ8OjrEPsdZCtHi1o5cG8mRr_zKiJfpX7bLCdH2gIgW84gjU9ukG89HbG56DrKgttXEakTOe-7_R4o6w1Dd8KKDwbpnm5JWg9OQL1JsXk3_gp-MsmyRLCsfv2LG4uMrERzwGf7HiXwWR-FWewME4bF7eI' \
-H 'Cookie: _ga=GA1.3.1338201315.1626928614; .AspNetCore.Antiforgery.KOtGzXzkvrY=CfDJ8OjrEPsdZCtHi1o5cG8mRr8L42pyDKYJe-I8UhMFnlmmxHJ5ncUZavhAzkZLqVQS0KMUPzUS3XvNNPA2zMkWeRj3sIwfCDpt8wgHLgq-UMJqawEWsePvk4KAKBat16PIked8BgFeQVEO53mvGMAUFmE; TS01a4606a=0117e34aded90e3cd45a7a68f29ff776b05afb2004de833c33d07eb015a3b394b53b040c99d33c80c73c61bea35e89429c460c63b1; _gid=GA1.3.763501256.1628127433; _gat_gtag_UA_731624_21=1' \
--data-raw 'param={"Unwanted":1,"Notifiable":1}'
