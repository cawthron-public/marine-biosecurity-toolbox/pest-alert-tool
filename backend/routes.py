from celery.result import AsyncResult
from fastapi import Body
from fastapi.responses import JSONResponse
from fastapi import APIRouter, File, UploadFile
from pydantic import BaseModel
from worker import blast_asv_task
import time
import os
import uuid
import datetime
import ncbi

# Router and Routes
router = APIRouter()

# get database info - when it was last downloaded
path = r"db/NIS.rRNA18S.db.final.fasta"
ti_m = os.path.getmtime(path)
m_ti = time.ctime(ti_m)

# Using the timestamp string to create a
# time object/structure
t_obj = time.strptime(m_ti)

# Transforming the time object to a timestamp
# of ISO 8601 format
T_stamp = time.strftime("%Y-%m-%d", t_obj)

'''Check matches'''

### get the number of nis sp and unwanted sp:

# NIS sp:
file = open("db/nis_18sdb_match.txt",'r')
data = file.read()
words = data.split("\n")
number = len(words) - 1

# unwanted sp:
file_u = open("db/unwanted_18sdb_match.txt",'r')
data_u = file_u.read()
words_u = data_u.split("\n")
number_u = len(words_u) - 1


@router.get("/nis")
async def getListOfNIS():
   

    response = {"NIS": ["Acantholobulus_pacificus", "Acanthophora_spicifera", "Acanthurus_xanthopterus", "Acentrogobius_pflaumii",
                        "Actumnus_setifer", "Alexandrium_minutum", "Alpheus_parasocialis", "Amathia_chimonidesi", "Amathia_distans", "Amathia_gracilis",
                        "Amathia_imbricata", "Amathia_verticillata", "Amphibalanus_amphitrite", "Amphibalanus_eburneus", "Amphibalanus_improvisus",
                        "Amphibalanus_reticulatus", "Amphibalanus_venustus", "Amphibalanus_zhujiangensis", "Amphilectus_fucorum", "Amphisbetia_maplestonei",
                        "Anemonia_manjano", "Anguinella_palmata", "Anomia_chinensis", "Antithamnionella_ternifolia", "Apocorophium_acutum", "Arbocuspis_bellula",
                        "Arbopercula_angulata", "Arcuatula_senhousia", "Arenigobius_bifrenatus", "Ascidiella_aspersa", "Asperococcus_bullosus", "Asperococcus_ensiformis",
                        "Asterias_forbesi", "Aulacomya_atra", "Austrobalanus_imperator", "Austromegabalanus_cylindricus", "Austromegabalanus_nigrescens",
                        "Austromegabalanus_psittacus", "Austrominius_covertus", "Balanus_rostratus", "Barantolla_lepte", "Barentsia_matsushimana",
                        "Bhawania_bhawania", "Biflustra_grandicella", "Boccardia_proboscidea", "Boccardia_pseudonatrix", "Bonamia_sp.", "Botrylloides_giganteum",
                        "Botrylloides_leachii", "Botryllus_schlosseri", "Botryllus_tuberatus", "Brettiella_culmosa", "Bugula_neritina", "Bugula_vectifera", "Bugulina_flabellata",
                        "Bugulina_simplex", "Bugulina_stolonifera", "Buskia_socialis", "Calliactis_polypus", "Callyspongia_robusta", "Cancer_pagurus", "Caprella_andreae",
                        "Caprella_californica", "Caprella_mutica", "Caprella_penantis", "Caprella_scauroides", "Carcinoscorpius_rotundicauda", "Carupa_tenuipes",
                        "Celleporaria_aperta", "Celleporaria_inaudita", "Celleporaria_nodulosa", "Celleporaria_pilaefera", "Celleporaria_sibogae", "Celleporaria_sp.",
                        "Celleporaria_umbonatoidea", "Chaetodon_trichrous", "Chaetomorpha_crassa", "Chaetomorpha_linum", "Champia_affinis", "Charybdis_hellerii",
                        "Charybdis_japonica", "Chironemus_maculosus", "Chnoospora_minima", "Chondria_harveyana", "Chondropsis_topsenti", "Chthamalus_challengeri",
                        "Chthamalus_dalli", "Chthamalus_fissus", "Chthamalus_fragilis", "Chthamalus_moro", "Chthamalus_panamensis", "Chthamalus_sinensis",
                        "Cilicaeopsis_whiteleggei", "Ciona_intestinalis", "Ciona_savignyi", "Cirolana_willeyi", "Cirolana_harfordi", "Cirrholovenia_tetranema",
                        "Cladonema_radiatum", "Cladophora_ruchingeri", "Clathria_prolifera", "Clavelina_lepadiformis", "Clytia_linearis", "Cnemidocarpa_hemprichi", "Cnemidocarpa_stolonifera", "Codium_fragile", "Colpomenia_bullosa", "Conopeum_papillorum", "Conopeum_ponticum", "Conopeum_seurati", "Cordylophora_caspia", "Corella_eumyota", "Coryne_eximia", "Coryne_japonica", "Coryne_pusilla", "Cradoscrupocellaria_bertholletii", "Crassicorophium_bonellii", "Crassimarginatella_xiamenensis", "Cryptosula_pallasiana", "Cutleria_multifida", "Cyclicopora_longipora", "Cymodoce_tuberculata", "Dactylia_varia", "Dendostrea_folium", "Dendostrea_rosacea", "Dendya_clathrata", "Diadema_setosum", "Diadumene_lineata", "Dictyota_furcellata", "Didemnum_vexillum", "Diplosoma_listerianum", "Diplosoma_ooru", "Dipolydora_armata", "Dipolydora_giardi", "Distaplia_viridis", "Dynoides_amblysinus", "Dynoides_dentisinus", "Ectopleura_crocea", "Ectopleura_dumortierii", "Ectopleura_exxonia", "Ectopleura_larynx", "Elasmopus_rapax", "Elasmopus_rapax", "Electra_zhoushanica", "Elodea_canadensis", "Elphidium_vellai", "Endeis_flaccida"]
                + ["Engraulis_japonicus", "Ercolania_boodleae", "Ericthonius_pugnax", "Ericthonius_punctatus", "Erosaria_erosa", "Euchone_limnicola", "Eucidaris_metularia", "Eudendrium_capillare", "Eudendrium_generale", "Eudistoma_elongatum", "Euraphia_calcareobasis", "Ficopomatus_enigmaticus", "Filellum_serpens", "Fistulobalanus_albicostatus", "Fistulobalanus_dentivarians", "Fistulobalanus_kondakovi", "Fistulobalanus_pallidus", "Galathea_spinosorostris", "Galeolaria_caespitosa", "Glebocarcinus_amphioetus", "Gobiopterus_semivestitus", "Gracilaria_sp.", "Grantessa_intusarticulata", "Grateloupia_subpectinata", "Grateloupia_turuturu", "Griffithsia_crassiuscula", "Hachijopagurus_rubrimaculatus", "Halecium_muricatum", "Halisarca_dujardinii", "Heniochus_acuminatus", "Heniochus_monoceros", "Herdmania_momus", "Hippopodina_feegeensis", "Hippoporina_indica", "Holopsamma_crassa", "Holothuria_impatiens", "Thymiosycia_impatiens", "Homarus_gammarus", "Hoplangia_durotrix", "Hydroclathrus_clathratus", "Hydroides_albiceps", "Hydroides_crucigera", "Hydroides_dirampha", "Hydroides_elegans", "Hydroides_ezoensis", "Hydroides_longispinosa", "Hydroides_perezi", "Hydroides_sanctaecrucis", "Hymeniacidon_agminata", "Hyotissa_hyotis", "Hyotissa_quercina", "Janolus_hyalinus", "Jassa_marmorata", "Jassa_marmorata", "Jassa_slatteryi", "Jassa_staudei", "Laticorophium_baconi", "Lepas_anserifera", "Anatifa_hillii", "Lepas_hillii", "Anatifa_anserifera", "Leucandra_compacta", "Ligia_exotica", "Megaligia_exotica", "Limaria_orientalis", "Limnoria_rugosissima", "Limnoria_tripunctata",
                    "Limulus_polyphemus", "Loxoconcha_parvifoveata", "Lysidice_unicornis", "Lysmata_californica", "Crassostrea_gigas", "Magallana_gigas", "Mallacoota_insignis", "Megabalanus_californicus", "Megabalanus_coccopoma", "Megabalanus_crispatus", "Megabalanus_dorbignii", "Megabalanus_occator", "Megabalanus_peninsularis", "Megabalanus_rosa", "Megabalanus_tintinnabulum", "Megabalanus_tulipiformis", "Megabalanus_vinaceus", "Megabalanus_volcano", "Megabalanus_zebra", "Melita_matilda", "Membraniporopsis_tubigerum", "Metapenaeus_bennettae", "Metopograpsus_frontalis", "Metopograpsus_latifrons", "Microcosmus_squamiger", "Mimachlamys_gloriosa", "Mimachlamys_sanguinea", "Modiolus_trailii", "Molgula_complanata", "Molgula_manhattensis", "Monanchora_clathrata", "Monocorophium_acherusicum", "Monocorophium_sextonae", "Mycale_toxifera", "Neosiphonia_harveyi", "Carmia_toxifera", "Mycale_stecarmia", "Oxymycale_stecarmia", "Myomenippe_hardwickii", "Myripristis_amaena", "Myripristis_berndti", "Myripristis_murdjan", "Mytilus_planulatus", "Nanosesarma_minutum", "Neanthes", "Neosiphonia_subtilissima", "Neosiphonia_harveyi", "Neosiphonia_sertularioides", "Neosiphonia_subtilissima", "Neotrapezium_sublaevigatum", "Nereis_pelagica", "Nevianipora_pulcherrima", "Newmanella_radiata", "Notomegabalanus_algicola", "Obelia_geniculata", "Obelia_longissima", "Okenia_eolida", "Okenia_pellucida", "Omobranchus_anolius", "Oncorhynchus_tshawytscha", "Ophiactis_savignyi", "Ophioclastus_hataii", "Ophiocoma_erinaceus", "Ophiocoma_pusilla", "Ophionereis_porrecta"]
                + ["Caulerpa_brachypus", "Caulerpa_parvafolia"]
                + ["Oratosquilla_oratoria", "Ostrea_angasi", "Ostrea_edulis", "Pachygrapsus_laevimanus", "Pachygrapsus_transversus", "Paracerceis_sculpta", "Paracorophium_brisbanensis", "Paralepidonotus_ampulliferus", "Parasmittina_pinctatae", "Parioglossus_marginalis", "Patro_australis", "Penaeus_canaliculatus", "Pennaria_disticha", "Perforatus_perforatus", "Perna_perna", "Pherecardia_striata", "Phoronis_ijimai", "Phyllorhiza_punctata", "Phyllotricha_verruculosa", "Pilumnus_minutus", "Pinctada_maculata", "Pinctada_imbricata", "Plicatula_australis", "Plumularia_pulchella", "Plumularia_setacea", "Polyandrocarpa", "Polyandrocarpa_zorritensis", "Polycera_hedgpethi", "Polydora_cornuta", "Polydora_haswelli", "Polydora_hoplura", "Polydora_websteri", "Polysiphonia_brodiei", "Polysiphonia_constricta", "Polysiphonia_morrowii", "Polysiphonia_senticulosa", "Pseudoctomeris_sulcata", "Pseudopolydora_corniculata", "Pseudopolydora_paucibranchiata", "Punctaria_latifolia", "Pyromaia_tuberculata", "Pyropia_koreana", "Pyura_dalbyi", "Pyura_doppelgangera", "Pyura_elongata", "Clathriodendron_arbuscula", "Raspailia_arbuscula", "Romaleon_gibbosulum", "Rosenvingea_sanctae-crucis", "Sabella_spallanzanii", "Sabella_pavonina", "Sabellastarte_spectabilis", "Sardinops_sagax_melanostictus", "Sargocentron", "Sargocentron_diadema", "Sargocentron_punctatissimum", "Savignyella_lafontii", "Scaeochlamys_squamata", "Schizophroida_hilensis", "Schizophrys_aspera", "Schizoporella_errata",
                   "Schizoporella_japonica", "Schizoporella_variabilis", "Schizymenia_apoda", "Scophthalmus_maximus", "Scrupocellaria_sp.", "Ser_fukiensis", "Sinoflustra_amoyensis", "Sinoflustra_annae", "Siphogenerina_raphana", "Solieria_sp.", "Spartina_alterniflora", "Spartina_townsendii_var._anglica", "Spartina_townsendi", "Sphaeroma_walkeri", "Sphaerozius_nitidus", "Spirobranchus_tetraceros", "Stenopus_hispidus", "Stenothoe_gallensis", "Stictyosiphon_soriferus", "Striaria_attenuata", "Striatobalanus_amaryllis", "Striatobalanus_tenuis", "Styela_canopus", "Styela_plicata", "Styela_clava", "Syllidia_hongkongensis", "Symplectoscyphus_indivisus", "Symplegma_viride", "Symplegma_brakenhielmi", "Tarsocryptus_laboriosus", "Teredora_princesae", "Tesseropora_rapax", "Tethocyathus_cylindraceus", "Tethya_bergquistae", "Tethya_multistella", "Tetraclita_japonica", "Tetraclita_japonica_japonica", "Tetraclita_squamosa", "Tetraclitella_coerulescens", "Tetraclitella_multicostata", "Tetraclitella_pilsbryi", "Thalamoporella_californica", "Thalamoporella_distorta", "Thalamoporella_sp.", "Theora_lubrica", "Tricellaria_catalinensis", "Tricellaria_inopinata", "Tritia_burchardi", "Ulva_australis", "Ulva_californica", "Ulva_compressa", "Ulva_flexuosa", "Ulva_intestinalis", "Ulva_lactuca", "Ulva_rigida", "Ulva_sp.", "Umbraulva_dangeardii", "Undaria_pinnatifida", "Virgulinella_fragilis", "Vosmaeropsis_macera", "Watersipora_arcuata", "Watersipora_subatra", "Watersipora_subtorquata"],
                "count": number,
                "timestamp": T_stamp
                }
    return response


@router.get("/unwanted")
async def getListOfunwanted():
    response = {"unwanted": [
        "Asterias_amurensis",
        "Carcinus_maenas",
        "Caulerpa_taxifolia",
        "Eriocheir_sinensis",
        "Haliotis_rufescens",
        "Potamocorbula_amurensis",
        "Sabella_spallanzanii",
        "Caulerpa_brachypus",
        "Caulerpa_parvafolia",
    ],
        "count": number_u,
        "timestamp": T_stamp
    }
    return response


class ASVRequest(BaseModel):
    header: str = ""
    sequence: str = ""


@router.post("/submitASV")
async def submitASV(request: ASVRequest):
    data = {'header': request.header, 'sequence': request.sequence}
    job_id = ncbi.submit_ASV(data)
    return job_id


def submitFile(content, geneDbName='NIS.rRNA18S.db.final.fasta'):
    out_file_path = str(uuid.uuid4()) + \
        datetime.datetime.utcnow().isoformat()[:10]+'.fa'
    task = blast_asv_task.delay(
        out_file_path, content.decode("utf-8"), geneDbName)
    return JSONResponse({"task_id": task.id})


@router.post("/18s", status_code=201)
async def post_18s_endpoint(in_file: UploadFile = File(...)):
    return submitFile(await in_file.read(), 'NIS.rRNA18S.db.final.fasta')


@router.post("/coi", status_code=201)
async def post_coi_endpoint(in_file: UploadFile = File(...)):
    return submitFile(await in_file.read(), 'NIS.COI.db.final.fasta')


@router.get("/tasks/{task_id}")
def get_status(task_id):
    task_result = AsyncResult(task_id)
    result = {
        "task_id": task_id,
        "task_status": task_result.status,
        "task_result": task_result.result
    }
    return JSONResponse(result)
