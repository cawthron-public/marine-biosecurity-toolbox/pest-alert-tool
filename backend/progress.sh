



# $(tail -n 1 progress.txt | awk '{print $1}')
# watch -n 0.1 "expr $(grep -n 'ASV_100' query/82aaff47-a3c7-4da8-8e59-b4d7deebcdb92021-08-10.fa | sed 's/:.*$//') / 2"
# watch -n 0.1 "expr $(grep -n $(tail -n 1 progress.txt | awk '{print $1}') query/82aaff47-a3c7-4da8-8e59-b4d7deebcdb92021-08-10.fa | sed 's/:.*$//') / 2 | echo"



line=$(tail -n 1 progress.txt | awk '{print $1}')
queryFile="../assets/SEA202018S_ASVs.fa"

# queryFile="query/82aaff47-a3c7-4da8-8e59-b4d7deebcdb92021-08-10.fa"
queryFileLength=$(wc -l $queryFile | awk '{print $1}')

echo $line $queryFileLength

expr $(grep -n $line $queryFile | sed 's/:.*$//') \* 100 /  $queryFileLength


