'''
 fastapi
 Max Scheel 2021 max@max.ac.nz
'''

import routes
from fastapi import FastAPI


# import orjson
# from typing import Any
# from starlette.responses import JSONResponse
# class ORJSONResponse(JSONResponse):
#     media_type = "application/json"

#     def render(self, content: Any) -> bytes:
#         return orjson.dumps(content)


def create_app():
    app = FastAPI(debug=True, root_path="/api/")

    # app = FastAPI(default_response_class=ORJSONResponse, debug=True)
    app.version = '1.0.0'
    app.include_router(routes.router)
    return app


app = create_app()
