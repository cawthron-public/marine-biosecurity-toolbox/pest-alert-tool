#!/usr/bin/env bash

mkdir -p /var/log/celery
mkdir -p /usr/src/app/query
mkdir -p /usr/src/app/results
chown -R nobody:nogroup /var/log/celery /usr/src/app/query /usr/src/app/results

celery worker  --app=worker.celery --loglevel=info --logfile=/var/log/celery/celery.log  #--uid=nobody --gid=nogroup
