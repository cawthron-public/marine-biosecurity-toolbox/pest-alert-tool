#!/bin/bash
mkdir -p results query

blastn \
	-outfmt 6 \
	-max_target_seqs 5 \
	-db $(pwd)/db/$2 \
	-query $(pwd)/query/$1 \
	-out $(pwd)/results/$1.txt \
	-num_threads 2

cat $(pwd)/header.txt $(pwd)/results/$1.txt | csv2json -t -d -
rm -f $(pwd)/query/$1 $(pwd)/results/$1.txt
