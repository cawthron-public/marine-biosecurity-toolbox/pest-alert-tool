import os
import json
import subprocess
from celery import Celery


def blastAway(queryFileName, geneDbName):
    p = subprocess.Popen(['./run_local.sh', queryFileName, geneDbName],
                         stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT)
    out, err = p.communicate()
    return out


CONFIG = {
    "CELERY_TASK_RESULT_EXPIRES": 10,
    "CELERY_BROKER_URL": os.environ.get("CELERY_BROKER_URL", "redis://localhost:6379"),
    "CELERY_RESULT_BACKEND": os.environ.get("CELERY_RESULT_BACKEND", "redis://localhost:6379")
}

celery = Celery(__name__, config=CONFIG)


@celery.task(name="blast_asv_task")
def blast_asv_task(out_file_path, content, geneDbName):
    with open('query/'+out_file_path, 'wb') as out_file:
        out_file.write(bytes(content, 'utf-8'))  # async write
    t = blastAway(out_file_path, geneDbName)
    response = json.loads(t)
    return response
