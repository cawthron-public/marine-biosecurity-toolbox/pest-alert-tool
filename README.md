# Pest Alert Tool - Marine Biosecurity Toolbox

This is the repository for the Pest Alert Tool (PAT). This tool can screen nuclear small subunit 18S ribosomal RNA (18S rRNA) and cytochrome oxidase subunit 1 (COI) datasets for marine non-indigenous species as well as unwanted and notifiable marine organisms in New Zealand. The output can be filtered by minimum length of the query sequence and identity match. For putative matches, a phylogenetic tree can be generated through NCBI’s Blast Tree View tool, allowing for additional verification of the species of concern detection.

## Running the app locally

First, follow the steps described in `db_update/README.md` to prepare the two reference databases that the FASTA files will be blasted against and to update information in the *Disclaimer* section based on the latest database version. 

Then, run the following lines to build the tool locally:

`docker-compose build && docker-compose up`

## Stack

### Backend
Docker-compose

( Auto certs and domain. on oracle)
- nginx
  - cors
  - certbotletsencrypt

- nginx:
   - cache ratelimit
   - CORS
   - basic auth for worker dashboard
   - serve static content (frontend)

- API python (fastapi)
- Redis message Queue
- worker python (celery)
- dashboard python (flower celery worker stats) pw protected

### Frontend

- Vue.js
- Vuetify
- Axios




