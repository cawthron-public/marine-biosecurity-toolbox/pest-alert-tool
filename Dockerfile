FROM node:17 as build-stage
WORKDIR /app
COPY frontend/package*.json ./
RUN yarn
RUN unset NODE_OPTIONS
ENV NODE_OPTIONS=--openssl-legacy-provider
COPY frontend/ .
RUN unset NODE_OPTIONS

RUN yarn build

FROM nginx:1.21.1

RUN mkdir -p /www/data

COPY --from=build-stage /app/dist /www/data
COPY nginx/htpasswd /etc/nginx/.htpasswd
COPY nginx/nginx.conf /etc/nginx/nginx.conf
