import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#00549e',
        secondary: '#01a8ba',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#01a8ba',
        success: '#4CAF50',
        warning: '#FFC107'
      },
    },
  },
});
