### **Privacy Policy/Cookie Policy**

We care about your privacy and the security of your personal data. This policy outlines what information we collect, how we use it, and how we protect it. By using our website, you consent to this policy.

### **Information we collect**

We may collect information about you when you use our website, such as your name, email address, and IP address. We may also collect information about your device, such as its type, operating system, and browser.

### **How we use your information**

We may use your information to improve our website, communicate with you, and provide you with the services you request. We may also use your information to personalize your experience.

### **Cookies**

Our website may use cookies to collect information about your browsing behavior. Cookies are small text files that are stored on your device. They allow us to recognize your device, remember your preferences, and provide a better user experience.

### **Third-party services**

We may use third-party services, such as Google Analytics, to analyze your use of our website. These services may also collect information about your browsing behavior.

### **Data protection**

We take data protection seriously and use appropriate measures to secure your personal data. We do not sell or rent your personal data to third parties.

### **Changes to this policy**

We may update this policy from time to time. We will post any changes to our website and notify you if required by law.
If you have any questions or concerns about our privacy policy or cookie policy, please contact us.