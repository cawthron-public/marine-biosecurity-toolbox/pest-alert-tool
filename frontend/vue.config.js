module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  css: {
    loaderOptions: {
      sass: {
        implementation: require("sass"),

        additionalData: "@import '@/assets/css/overwrites.scss'",
      },
      scss: {
        additionalData: "@import '@/assets/css/overwrites.scss';",
      },
    },
  },
}
