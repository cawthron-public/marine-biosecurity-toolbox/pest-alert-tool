# Steps to update the db used in the Pest Alert Tool - PAT:

## Step 1 - Inside db_update/db/referencedb folder run the following R lines:
    library(taxonomizr) 
    #note this will require a lot of hard drive space, bandwidth and time to process all the data from NCBI
    prepareDatabase('accessionTaxa.sql')

## Step 2 - update the db:

go to the `db` folder and run all lines in the Makefile - make file does not run automatically, it needs tidy up, you will have to read the comments and copy and paste the chuncks in the terminal (starting from unzipping the folders from Step 1). At the end you should copy all files with the following prefix:
    
    NIS.COI.db.final.fasta
    NIS.rRNA18S.db.final.fasta

to **../backend/db**

## Step 3 - generate csv with matches"

Run `./make save_matches` and copy `unwanted_18sdb_match.txt` and `nis_18sdb_match.txt` files to folder `../backend/db` and matches also to `../frontend/public/examples`. Now you can redeploy PAT with the database and disclaimer info updated.