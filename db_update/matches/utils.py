'Extract all sp listed in the 18S DB'
def extract_sp_18sdb():
    # defining object file1 open in read mode
    file1 = open('db/NIS.rRNA18S.db.final.fasta', 'r')
    
    # defining object file2 open in write mode
    file2 = open('examples/sp_18sdb.txt', 'w')
    
    # reading each line from original
    # text file
    for line in file1.readlines():
    
        # reading all lines that begin with ">"
        if (line.startswith('>')):
        
            # remove special characters
            line = line.replace('>', '')

            # "_" should be empty space to match with list of sp from Anastasija:
            line = line.replace('_', ' ')

            # keep only the sp name:
            if ";" in line:
                line = line.split(";")[1]
            
            # storing only those lines that begin with ">"
            file2.write(line)

    # close and save the files
    file2.close()
    file1.close()

'List of sp in the NIS/unwanted sp available in the 18S DB:'
def compare_list_with_db(first_file_list, second_file_list, outfile):
    new_lines = []
    for item in second_file_list:
        if item in first_file_list:
            new_lines.append(item)

    with open(outfile,'w') as file_out:
        for line in new_lines:
            file_out.write(line)