from utils import compare_list_with_db, extract_sp_18sdb

# extract only the sp names from the 18S DB:
extract_sp_18sdb()

# first file will always be the list of sp name extracted from the 18S db 
# (todo: check with Ana if we should also do the same inclusing the sp in the COI DB)

first_file_list = open('examples/sp_18sdb.txt','r').readlines()

## comparing nis:
second_file_list = open('examples/NIS_list_22-11-17.txt','r').readlines()
compare_list_with_db(first_file_list, second_file_list, outfile="examples/nis_18sdb_match.txt")

## comparing unwanted sp:
second_file_list = open('examples/Unwanted_notifiable_22-11-17.txt','r').readlines()
compare_list_with_db(first_file_list, second_file_list, outfile="examples/unwanted_18sdb_match.txt")


### get the number of nis sp and unwanted sp:

# NIS sp:
file = open("examples/nis_18sdb_match.txt",'r')
data = file.read()
words = data.split("\n")
number = len(words)
print(number)

# unwanted sp:
file_u = open("examples/unwanted_18sdb_match.txt",'r')
data_u = file_u.read()
words_u = data_u.split("\n")
number_u = len(words_u)
print(number_u)